package org.bobby.processors.stellar;

import java.util.concurrent.Callable;

import org.stellar.sdk.AssetTypeNative;
import org.stellar.sdk.KeyPair;
import org.stellar.sdk.MemoText;
import org.stellar.sdk.Network;
import org.stellar.sdk.PaymentOperation;
import org.stellar.sdk.Server;
import org.stellar.sdk.Transaction;
import org.stellar.sdk.responses.AccountResponse;
import org.stellar.sdk.responses.SubmitTransactionResponse;

public class PutStellarWorker implements Callable<SubmitTransactionResponse>{
	
	private Server server;
	private String sourceAccountId;
	private String targetAccountId;
	private String amount;
	private String memo;
	private Integer timeout=180;
	private boolean isPublicNetwork;
	
	public PutStellarWorker(Server server, String sourceAccountId, String targetAccountId, String amount, String memo,
			Integer timeout, boolean isPublicNetwork) {
		super();
		this.server = server;
		this.sourceAccountId = sourceAccountId;
		this.targetAccountId = targetAccountId;
		this.amount = amount;
		this.memo = memo;
		this.timeout = timeout;
		this.isPublicNetwork = isPublicNetwork;
	}

	@Override
	public SubmitTransactionResponse call() throws Exception {
		System.out.println("--- Issuing transaction from " + sourceAccountId + " to " + targetAccountId);
		
		SubmitTransactionResponse response;
		KeyPair source = KeyPair.fromAccountId(sourceAccountId);
		KeyPair destination = KeyPair.fromAccountId(targetAccountId);

		Network usedNetwork = (isPublicNetwork ? Network.PUBLIC : Network.TESTNET);

		try {
			server.accounts().account(destination.getAccountId());
			AccountResponse sourceAccount = server.accounts().account(source.getAccountId());

			Transaction transaction = new Transaction.Builder(sourceAccount, usedNetwork).addOperation(
					new PaymentOperation.Builder(destination.getAccountId(), new AssetTypeNative(), amount).build())
					.addMemo(MemoText.text(memo)).setTimeout(timeout).build();
			transaction.sign(source);

			response = server.submitTransaction(transaction);
			
			System.out.println("--- Transaction "+response.getHash()+ " is success ->"+ response.isSuccess());
		} finally {
			server.close();
		}
		
		return response;
	}

}
