/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bobby.processors.stellar;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.atomic.AtomicReference;

import org.apache.commons.io.IOUtils;
import org.apache.nifi.annotation.behavior.InputRequirement;
import org.apache.nifi.annotation.behavior.InputRequirement.Requirement;
import org.apache.nifi.annotation.behavior.ReadsAttribute;
import org.apache.nifi.annotation.behavior.ReadsAttributes;
import org.apache.nifi.annotation.behavior.WritesAttribute;
import org.apache.nifi.annotation.behavior.WritesAttributes;
import org.apache.nifi.annotation.documentation.CapabilityDescription;
import org.apache.nifi.annotation.documentation.SeeAlso;
import org.apache.nifi.annotation.documentation.Tags;
import org.apache.nifi.annotation.lifecycle.OnScheduled;
import org.apache.nifi.annotation.lifecycle.OnStopped;
import org.apache.nifi.components.PropertyDescriptor;
import org.apache.nifi.flowfile.FlowFile;
import org.apache.nifi.logging.ComponentLog;
import org.apache.nifi.processor.AbstractProcessor;
import org.apache.nifi.processor.ProcessContext;
import org.apache.nifi.processor.ProcessSession;
import org.apache.nifi.processor.ProcessorInitializationContext;
import org.apache.nifi.processor.Relationship;
import org.apache.nifi.processor.exception.ProcessException;
import org.apache.nifi.processor.io.InputStreamCallback;
import org.apache.nifi.processor.util.StandardValidators;
import org.json.JSONArray;
import org.json.JSONObject;
import org.stellar.sdk.responses.SubmitTransactionResponse;

import com.google.gson.Gson;

@Tags({"stellar","put"})
@CapabilityDescription("Provide a description")
@SeeAlso({})
@ReadsAttributes({@ReadsAttribute(attribute="", description="")})
@WritesAttributes({@WritesAttribute(attribute="", description="")})
@InputRequirement(Requirement.INPUT_REQUIRED)
public class PutStellar extends AbstractProcessor {
	
	private ComponentLog logger;

    public static final PropertyDescriptor STELLAR_NET_URI = new PropertyDescriptor
            .Builder().name("STELLAR_NET_URI")
            .displayName("Stellar Net URI")
            .description("Location of Stellar Network, default is https://horizon-testnet.stellar.org")
            .required(true)
            .defaultValue("https://horizon-testnet.stellar.org")
            .addValidator(StandardValidators.NON_EMPTY_VALIDATOR)
            .build();
    public static final PropertyDescriptor STELLAR_NET_TYPE = new PropertyDescriptor
            .Builder().name("STELLAR_NET_TYPE")
            .displayName("Stellar Net Type")
            .allowableValues("PUBLIC","TEST")
            .description("Uncheck this if using public net")
            .required(true)
            .defaultValue("TEST")
            .addValidator(StandardValidators.NON_EMPTY_VALIDATOR)
            .build();

    public static final Relationship SUCCESS = new Relationship.Builder()
            .name("Success")
            .description("If process ran succesfully, output will be thrown to this relationship")
            .build();
    public static final Relationship ERROR = new Relationship.Builder()
            .name("Error")
            .description("If process ran unsuccesfully, output will be thrown to this relationship")
            .build();

    private List<PropertyDescriptor> descriptors;

    private Set<Relationship> relationships;
    
    private PutStellarManager putStellarManager;
    
    @Override
    protected void init(final ProcessorInitializationContext context) {
        final List<PropertyDescriptor> descriptors = new ArrayList<PropertyDescriptor>();
        descriptors.add(STELLAR_NET_URI);
        descriptors.add(STELLAR_NET_TYPE);
        this.descriptors = Collections.unmodifiableList(descriptors);

        final Set<Relationship> relationships = new HashSet<Relationship>();
        relationships.add(SUCCESS);
        relationships.add(ERROR);
        this.relationships = Collections.unmodifiableSet(relationships);
    }

    @Override
    public Set<Relationship> getRelationships() {
        return this.relationships;
    }

    @Override
    public final List<PropertyDescriptor> getSupportedPropertyDescriptors() {
        return descriptors;
    }

    @OnScheduled
    public void onScheduled(final ProcessContext context) {
    	final String StellarNetUri = context.getProperty(STELLAR_NET_URI).getValue();
    	this.logger = this.getLogger();
    	this.putStellarManager = new PutStellarManager(StellarNetUri, logger);
    }
    
    @OnStopped
    public void OnStopped() {
    	this.putStellarManager.destroy();
    }

    @Override
    public void onTrigger(final ProcessContext context, final ProcessSession session) throws ProcessException {
    	FlowFile requestFlowFile = null;
		FlowFile responseFlowFile = null;

		final AtomicReference<JSONArray> atomicRef = new AtomicReference<>();
    	
        if (context.hasIncomingConnection()) {
        	requestFlowFile = session.get();

            // If we have no FlowFile, and all incoming connections are self-loops then we can continue on.
            // However, if we have no FlowFile and we have connections coming from other Processors, then
            // we know that we should run only if we have a FlowFile.
            if (requestFlowFile == null && context.hasNonLoopConnection()) {
                return;
            }
        }

        // create ff response
        responseFlowFile = session.create(requestFlowFile);
        
        // pull flow file content to atomic reference
        logger.info("-> Extracting request content");
        session.read(requestFlowFile, new InputStreamCallback() {
    		JSONArray jsonArray = null;
    			@Override
    			public void process(final InputStream in) {
    				try {
    					String result = IOUtils.toString(in, "UTF-8");
    					jsonArray = new JSONArray(result);
    					atomicRef.set(jsonArray);
    				} catch (Exception e) {
    					e.printStackTrace();
    				} 
    			}
   		});
        logger.info("-> Done extracting request content");
        
        JSONArray requestArray = atomicRef.get();
        
        if(requestArray.isEmpty()) {
        	logger.warn("-> Request content is empty, removing session");
        	session.remove(responseFlowFile);
        }
        else {
        	try {
        		JSONArray responseArr = new JSONArray();
        		
        		// submitting transaction
        		for(int i=0;i<requestArray.length();i++) {
        			JSONObject o = requestArray.getJSONObject(i);
        			logger.info("-> Processing "+o.toString(1));
        			SubmitTransactionResponse response = putStellarManager.submit(
        					o.getString("requestId"),
        					o.getString("sourceSecretSeed"), 
        					o.getString("targetAccountId"), 
        					o.getString("amount"), 
        					o.getString("memo"), 
        					o.getInt("timeout"), 
        					context.getProperty(STELLAR_NET_TYPE).getValue().equalsIgnoreCase("public") ? true : false);
        			
        			JSONObject responseObj = new JSONObject();
        			responseObj.put("requestId", o.getString("requestId"));
        			responseObj.put("transactionHash", response.getHash());
        			responseObj.put("isSuccess", response.isSuccess());
        			
        			responseArr.put(responseObj);
        		}
        		
        		responseFlowFile = session.write(responseFlowFile,
						out -> out.write(responseArr.toString(1).getBytes()));
				session.transfer(responseFlowFile, SUCCESS);
				
        	}
        	catch(Exception exception) {
        		exception.printStackTrace();
        		logger.error("-> Error processing request "+exception);
        		responseFlowFile = session.write(responseFlowFile,
						out -> out.write(new Gson().toJson(exception.getMessage()).getBytes()));
				session.transfer(responseFlowFile, ERROR);
        	}
        	finally {
        		if(null!=requestFlowFile) session.remove(requestFlowFile);
        		logger.info("-> Removed session flow file");
        	}
        }
        
    }
    
}
