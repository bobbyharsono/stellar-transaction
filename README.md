This is a simple project to implement Stellar's common transaction into [Nifi](https://nifi.apache.org/) Processor;

**Usage**
Download or clone the source and build + run as Maven project, i.e. clean install;
Copy NAR file from $Project/target folder to library directory of installed Nifi.

