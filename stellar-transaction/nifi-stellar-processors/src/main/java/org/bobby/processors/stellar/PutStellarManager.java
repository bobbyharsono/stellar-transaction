package org.bobby.processors.stellar;

import java.io.IOException;
import org.apache.nifi.logging.ComponentLog;
import org.stellar.sdk.AccountRequiresMemoException;
import org.stellar.sdk.AssetTypeNative;
import org.stellar.sdk.KeyPair;
import org.stellar.sdk.MemoText;
import org.stellar.sdk.Network;
import org.stellar.sdk.PaymentOperation;
import org.stellar.sdk.Server;
import org.stellar.sdk.Transaction;
import org.stellar.sdk.responses.AccountResponse;
import org.stellar.sdk.responses.SubmitTransactionResponse;

public class PutStellarManager {

	private Server server;
	private final ComponentLog logger;

	public PutStellarManager(String stellarNetUri, ComponentLog logger) {
		super();
		this.server = new Server(stellarNetUri);
		this.logger = logger;
		logger.info("--> Initiated Stellar Manager");
	}

	public void destroy() {
		this.server.close();
		logger.info("--> Destroyed Stellar Manager");
	}

	public SubmitTransactionResponse submit(String requestId, String sourceSecretSeed, String targetAccountId, String amount,
			String memo, Integer timeout, boolean isPublicNetwork) throws IOException, AccountRequiresMemoException {
		logger.info("--> Issuing transaction "+requestId+" from " + sourceSecretSeed + " to " + targetAccountId);

		SubmitTransactionResponse response;
		KeyPair source = KeyPair.fromSecretSeed(sourceSecretSeed);
		KeyPair destination = KeyPair.fromAccountId(targetAccountId);

		Network usedNetwork = (isPublicNetwork ? Network.PUBLIC : Network.TESTNET);

		try {
			server.accounts().account(destination.getAccountId());
			AccountResponse sourceAccount = server.accounts().account(source.getAccountId());

			Transaction transaction = new Transaction.Builder(sourceAccount, usedNetwork).addOperation(
					new PaymentOperation.Builder(destination.getAccountId(), new AssetTypeNative(), amount).build())
					.addMemo(MemoText.text(memo)).setTimeout(timeout).build();
			transaction.sign(source);

			response = server.submitTransaction(transaction);

			logger.info("--> Transaction "+requestId+ " response = " + response.getHash() + " & is success = " + response.isSuccess());
		} finally {
			server.close();
		}

		return response;
	}

}
